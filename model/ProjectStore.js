import Storage from './Storage';
import Project from './Project';
import AppCoordinator from '../common/AppCoordinator';

const kProjects = "Projects";

export default class ProjectStore extends Storage {    
    constructor() {
        super();
        console.log("constructor");
    }

    init() {
        console.log("init");
        AppCoordinator.projects = [];
    }

    getAllProjects(callback) {
        if (AppCoordinator.projects) {
            callback(true, AppCoordinator.projects);
        } else {
            try {
                var list = this.valueFor(kProjects).then((list) => {
                    if (list) {
                        var retList = JSON.parse(list);
                        AppCoordinator.projects = [];
                        retList.map( (project) => {
                            if (project) {
                                AppCoordinator.projects.push(new Project(project));
                            }
                        })
                        callback(true, AppCoordinator.projects);
                    } else {
                        callback(true, []);
                    }
                }).catch(() => {
                    callback(false, null);
                })
            } catch (error) {
                callback(false, null);
            }
        }
    }

    saveProject(project, callback) {
        try {
            if (!AppCoordinator.projects) {
                AppCoordinator.projects = [];
            }

            var foundProject = AppCoordinator.projects.findIndex((item) => {
                return item.id === project.id;
            });
      
            if (foundProject >= 0) {
                foundProject.id = project.id;
                foundProject.project_id = foundProject.project_id;
                foundProject.project_name = foundProject.project_name;
                foundProject.days = foundProject.days;
            } else {
                AppCoordinator.projects.push(project);
            }
            var listJSON = JSON.stringify(AppCoordinator.projects);
            this.storeValue(listJSON, kProjects).then(() => {
                callback(true);
            }).catch(() => {
                callback(false);
            })
        } catch (error) {
            callback(false);
        }
    }

    resetProjects() {
        try {
            AppCoordinator.projects = [];
            var listJSON = JSON.stringify(AppCoordinator.projects);
            this.storeValue(listJSON, kProjects).then(() => {
                callback(true);
            }).catch(() => {
                callback(false);
            })
        } catch (error) {
            callback(false);
        }
    }
}