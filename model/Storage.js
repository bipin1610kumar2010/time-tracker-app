import { AsyncStorage } from 'react-native';

export default class Storage {
    constructor() {

    }

    async valueFor(key) {
        try {
            let value = await AsyncStorage.getItem(key);
            return value;
        } catch (error) {
            return null;
        }
    }

    async storeValue(value, key) {
        try {
            await AsyncStorage.setItem(key, value);
            return true;
        } catch (error) {
            return false;
        }
    }

    async removeValue(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        } catch (error) {
            return false;
        }
    }

    clear() {
        try {
            AsyncStorage.clear();
            return true;
        } catch (error) {
            return false;
        }
    }
}