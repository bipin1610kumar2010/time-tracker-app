import moment from 'moment';
export default class Project {
    id = null;
    project_id = null;
    project_name = null;
    days = null;

    constructor(props) {
        if (props) {
            this.id = props.id || null;
            this.project_id = props.project_id || null;
            this.project_name = props.project_name || null;
            this.days = props.days || null;
        }
    }

    isTrackingInProgress() {
        if (this.days && this.days.length > 0) {
            var trackers = this.days.filter((timetracker) => {
                if (timetracker.start_timestamp && !timetracker.end_timestamp) {
                    return true;
                } else {
                    return false;
                }
            });

            if (trackers.length > 0) {
                return true;
            }
        } else {
            return false;
        }
    }

    startTracking() {
        if (!this.days) {
            this.days = [];
        }

        var tracking = new DayTracking();
        tracking.start_timestamp = Math.floor(Date.now() / 1000);
        this.days.push(tracking);
    }

    endTracking() {
        if (this.days && this.days.length > 0) {
            var trackers = this.days.filter((timetracker) => {
                if (timetracker.start_timestamp && !timetracker.end_timestamp) {
                    return true;
                } else {
                    return false;
                }
            });

            if (trackers.length > 0) {
                trackers.forEach(tracker => {
                    tracker.end_timestamp = Math.floor(Date.now() / 1000);
                });
            }
        }
    }
};

export class DayTracking {
    start_timestamp = null;
    end_timestamp = null;
    audi_file_path = null;

    constructor(props) {
        if (props) {
            this.start_timestamp = props.start_timestamp || null;
            this.end_timestamp = props.end_timestamp || null;
            this.audi_file_path = props.audi_file_path || null;
        }
    }
};