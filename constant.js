var constant = {};
constant.headerTitleColor = 'cornflowerblue';
constant.displayDateFormat = "DD/MM/YYYY HH:mm:ss a";

export default constant;