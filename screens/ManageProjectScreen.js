import React from 'react';
import ProjectStore from '../model/ProjectStore';
import Constant from '../constant';
import { StyleSheet, Text, View, Image, FlatList, TouchableHighlight } from 'react-native';
import Events from '../common/Event';

export default class ManageProjectScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
  const { params = {} } = navigation.state;
    let headerLeft = (
      <TouchableHighlight onPress={params.handleBack} underlayColor='#00000000'>
        <Image
          style={{ width: 20,
            left: 15,
            height: 20}}
          source={require('../images/back.png')}
        />
      </TouchableHighlight>
    );
    return { headerLeft: headerLeft, title: 'Manage Projects' };
  };

  _handleBack = () => {
    Events.publish('RefreshList');
    this.props.navigation.goBack();
  }

  projectStore = new ProjectStore;

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      message: "No Projects Added",
    };
  }

  componentDidMount() {
    this.refresh();
    this.props.navigation.setParams({ handleBack: this._handleBack});
    this.refreshEvent = Events.subscribe('RefreshList', () => this.refresh());
  }

  componentWillUnmount () {
    this.refreshEvent.remove();
  }    

  refresh() {
    var projects = this.projectStore.getAllProjects((status, list)=> {
      if (status && list) {
        this.setState({
          data: list
        })
      } else {
        this.setState({
          data: []
        })
      }
    })
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
        {
          this.state.data.length > 0 ? 
          this.renderProjects() : 
          this.renderMessage()
        }
        </View>
        <View style={ styles.bottomBar }> 
          <TouchableHighlight style={styles.bottomButtons} underlayColor="#cdcdcd22" onPress={() => this.props.navigation.navigate('ProjectDetail')}>
            <Text>New Project</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

    renderProjects() {
      return (
        <FlatList
          style={{flex: 1}}
          data={this.state.data}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item) => {
            return item.id;
          }}
        />
      );
    }

    _renderItem = ({item}) => {
      return (
      <View style={{flex: 1, flexDirection: 'row', padding: 7, left: 0, right: 0, alignItems: 'center',justifyContent: 'flex-start'}}>
        <TouchableHighlight style={{flex: 1,}} underlayColor='#00000000' onPress={() => {
          this.pressedProject(item.id);
        }}>
          <View style={{flex: 1, justifyContent: 'center', backgroundColor: 'white'}}>
              <Text style={{margin: 10, textAlign: 'center',}}>
                {item.project_name}
              </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  pressedProject() {

  }
  renderMessage() {
    return (
      <View style={{flex: 1, alignItems: 'center',justifyContent: 'center'}}>
        <Text>
          {this.state.message}
        </Text>
      </View>
    )
  }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    bottomBar: {
      height: 50,
      backgroundColor: 'deepskyblue',
      position: 'absolute', 
      bottom: 0,
      left: 0,
      right: 0,
      flexDirection: 'row'
    },
    bottomButtons: {
      alignItems:'center',
      justifyContent: 'center',
      flex:1,
    },
  });