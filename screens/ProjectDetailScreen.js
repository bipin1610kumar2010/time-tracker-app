import React from 'react';
import ProjectStore from '../model/ProjectStore';
import { StyleSheet, Text, View, TextInput, Button, Image, TouchableHighlight, Alert } from 'react-native';
import { Icon, Left } from 'react-navigation';
import Project from '../model/Project';
import Events from '../common/Event';
import Constant from '../constant';

export default class ManageProjectScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
      let headerLeft = (
        <TouchableHighlight onPress={params.handleBack} underlayColor='#00000000'>
          <Image
            style={{ width: 20,
              left: 15,
              height: 20}}
            source={require('../images/back.png')}
          />
        </TouchableHighlight>
      );
      return { headerLeft: headerLeft, title: 'Project' };
    };

    _handleBack = () => {
      Events.publish('RefreshList');
      this.props.navigation.goBack();
    }

    projectStore = new ProjectStore;
    
    constructor(props) {
      super(props);
      this.state = {
        text: props.navigation.state.project ? props.navigation.state.project.project_name : "",
        project: props.navigation.state.project,
      };
    }

    componentDidMount() {
      this.props.navigation.title = 'Project';
      this.props.navigation.setParams({ handleBack: this._handleBack});
      // var projects = this.projectStore.getAllProjects((status, list)=> {
      //   if (status && list) {
      //     this.setState({
      //       data: list
      //     })
      //   } else {
      //     this.setState({
      //       data: []
      //     })
      //   }
      // })
      // this.state
    }
        
    render() {
      return (
        <View style={{flex: 1}}>
          <View style={{padding: 10}}>
            <TextInput
              style={{height: 40}}
              placeholder="Project Name"
              onChangeText={(text) => this.setState({text})}
            />
            <Text style={{padding: 10, fontSize: 42}}>
              {

              }
            </Text>
          </View>
          <View style={ styles.bottomBar }> 
            <TouchableHighlight style={styles.bottomButtons} underlayColor="#cdcdcd22" onPress={() => 
              {
                if (this.state.text.trim().length == 0) {
                  this.showErroAlert();
                } else {
                  var project = this.props.navigation.state.project
                  if (!this.state.project) {
                    project = new Project();
                    var timeStamp = Math.floor(Date.now() / 1000);
                    project.id = Math.floor(timeStamp);
                  } 
                  project.project_name = this.state.text;
                  this.projectStore.saveProject(project, () => {
                    this.showAlert();
                  });
                }
              }}
              >
              <Text>Save</Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
    showAlert() {
      Alert.alert(
        'Time Tracking',
        'Project is saved.',
        [
          {
            text: 'Ok', 
            onPress: () => {
              Events.publish('RefreshList');
              this.props.navigation.goBack();
            }
          },
        ],
        { cancelable: false }
      )
    }

    showErroAlert() {
      Alert.alert(
        'Time Tracking',
        'Please enter the project name.',
        [
          {
            text: 'Ok', 
            onPress: () => {
              
            }
          },
        ],
        { cancelable: false }
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    backButton : {
      width: 33,
      height: 33
    },
    bottomBar: {
      height: 50,
      backgroundColor: 'deepskyblue',
      position: 'absolute', 
      bottom: 0,
      left: 0,
      right: 0,
      flexDirection: 'row'
    },
    bottomButtons: {
      alignItems:'center',
      justifyContent: 'center',
      flex:1,
    },
  });