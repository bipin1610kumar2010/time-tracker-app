import React from 'react';
import moment from 'moment';
import ProjectStore from '../model/ProjectStore';
import Constant from '../constant';
import { StyleSheet, Text, View, FlatList, Button, Image, Dimensions, TouchableHighlight } from 'react-native';
import Events from '../common/Event';
// import Prompt from 'react-native-prompt';

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const projectIcon = require('../images/project.png');
export default class HomeScreen extends React.Component {
    static navigationOptions = {
      title: 'Dashboard',
      headerTintColor: Constant.headerTitleColor
    };

    projectStore = new ProjectStore;

    constructor(props) {
      super(props);
      this.state = {
        data: [],
        promptVisible: false,
        message: "No Projects Added",
      };
    }
  
    componentDidMount() {
      this.refresh();
      this.refreshEvent = Events.subscribe('RefreshList', () => this.refresh());
    }

    componentWillUnmount () {
      this.refreshEvent.remove();
    }    

    refresh() {
      var projects = this.projectStore.getAllProjects((status, list)=> {
        if (status && list) {
          this.setState({
            data: list
          })
        } else {
          this.setState({
            data: []
          })
        }
      })
    }
    render() {
      return (
        <View style={{flex: 1}}>
          <View style={{flex: 1}}>
          {
            this.state.data.length > 0 ? 
            this.renderProjects() : 
            this.renderMessage()
          }
          </View>
          <View style={ styles.bottomBar }> 
            <TouchableHighlight style={styles.bottomButtons} underlayColor="#cdcdcd22" onPress={() => this.props.navigation.navigate('ManageProjects')}>
              <Text>Manage Projects</Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.bottomButtons} underlayColor="#cdcdcd22" onPress={() => this.props.navigation.navigate('Settings', {project: null})}>
              <Text>Settings</Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }

    renderMessage() {
      return (
        <View style={{flex: 1, alignItems: 'center',justifyContent: 'center'}}>
          <Text>
            {this.state.message}
          </Text>
        </View>
      )
    }

    renderProjects() {
      return (
        <FlatList
          style={{flex: 1}}
          data={this.state.data}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item) => {
            return item.id;
          }}
        />
      );
    }

    _renderItem = ({item}) => {
        return (
        <View style={{flex: 1, flexDirection: 'row', padding: 7, left: 0, right: 0, alignItems: 'center',justifyContent: 'flex-start'}}>
          <TouchableHighlight underlayColor='#00000000' onPress={() => {
            this.projectPressed(item.id);
          }}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center',justifyContent: 'flex-start'}}>
              <View style={{marginLeft: 12,borderColor: 'gray', borderWidth: 1, height: 10, width: 5}}>
              </View>
              <View style={{borderColor: 'gray', borderWidth: 1, borderRadius: 5,  width: 100, alignItems: 'center',justifyContent: 'center', backgroundColor: item.isTrackingInProgress() ? 'white' : '#3e6522'}}>
                <Text style={{margin: 10}}>
                  {item.project_name}
                </Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={{flex: 1,}} underlayColor='#00000000' onPress={() => {
            this.timeStampPressed(item.id);
          }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={{margin: 10, textAlign: 'center',}}>
                  {this.getTimeStampToDisplay(item.id)}
                </Text>
            </View>
          </TouchableHighlight>
        </View>
      );
    }

    getTimeStampToDisplay(itemID) {
      if (this.state.data) {
        var project = this.state.data.find((item) => {
          return item.id === itemID;
        });

        if (project && project.days && project.days.length > 0) {
          var timeTracking = project.days[0];
          var time = new Date(timeTracking.start_timestamp*1000); 
          var formatted = moment(Date(time)).format(Constant.displayDateFormat);
          return formatted;
        }
      }
    }

    projectPressed(itemID) {
      if (this.state.data) {
        var project = this.state.data.find((item) => {
          return item.id === itemID;
        });

        if (project) {
          if (project.isTrackingInProgress()) {
            //TODO: start recording
            project.endTracking();
          } else {
            project.startTracking();
          }

          this.projectStore.saveProject(project, () => {
            Events.publish('RefreshList');
          });
        }
      }
    }

    timeStampPressed(itemID) {
      if (this.state.data) {
        var project = this.state.data.find((item) => {
          return item.id === itemID;
        });

        

        this.projectStore.saveProject(project, () => {
          Events.publish('RefreshList');
        });
      }
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    bottomBar: {
      height: 50,
      backgroundColor: 'deepskyblue',
      position: 'absolute', 
      bottom: 0,
      left: 0,
      right: 0,
      flexDirection: 'row'
    },
    bottomButtons: {
      alignItems:'center',
      justifyContent: 'center',
      flex:1,
    },
  });