import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from './screens/HomeScreen';
import ManageProjectsScreen from './screens/ManageProjectScreen';
import ProjectDetailScreen from './screens/ProjectDetailScreen';
import SettingScreen from './screens/SettingScreen';
import { StackNavigator } from 'react-navigation';

export const AppStructure = StackNavigator({
  Home: { screen: HomeScreen },
  ManageProjects: { screen: ManageProjectsScreen },
  ProjectDetail: { screen: ProjectDetailScreen,  },
  Settings: { screen: SettingScreen,  },
});

export default class App extends React.Component {
  render() {
    return (<AppStructure />);
  }
}
